export class Image {
    src : string;
    id : number;
    like : number;
    dislike : number;
    title : string;
    extension : string;
    author : string;

    constructor () {
    }

    public setAuthor(name : string) : void {
        this.author = name;
    }
    public setExtension(ext : string) : void {
        this.extension = ext;
    }

    public getExtension() : string {
        return this.extension;
    }

    public getId() : number {
        return this.id;
    }

    public setSrc(src : string) : void {
        this.src = src;
    }

    public setId(id : number) : void {
        this.id = id;
    }

    public setLike(like : number) : void {
        this.like = like;
    }

    public setDislike(dislike : number) : void {
        this.dislike = dislike;
    }

    public setTitle(title : string) : void {
        this.title = title;
    }

    public getSrc() : string {
        return this.src;
    }

    public incrementLike() : void {
        this.like +=1;
    }

    public decrementLike() : void {
        this.dislike -=1;
    }
}

