import { Injectable} from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';

import { Subject }    from 'rxjs/Subject';
import 'rxjs/add/operator/toPromise';

import { UserInfoService } from '../auth/user-info.service';
import { Image } from '../../classes/image';

@Injectable()
export class ImageAPIService {

    url : string = 'https://jsonplaceholder.typicode.com/comments';
    urlUpload : string = 'http://frenchinstagram2308.cloudapp.net:8888/albums/';

    constructor(private http: Http,private userInfoSerivce : UserInfoService) { 
    }

    getSearchResults() : void {

        // this.launchSearch(requestBody,requestUrl).then(result => this.facetteDataService.interpretFacetteData(result));
    }


    getImage() : Promise<any> {
      let headers = new Headers({ 'Content-Type': 'image/jpeg' });
      let options = new RequestOptions({ headers: headers });
      return this.http.get('http://lorempixel.com/g/400/200/')
               .toPromise()
               .catch(this.handleError);
    }


    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    getHomeSreenImages() : Promise<any> {
      let customUrl = 'http://frenchinstagram2308.cloudapp.net:8888/image/getRandomImage';
      return this.http.get(customUrl)
               .toPromise()
               .catch(this.handleError);
    }

    getSingleImage(id : number) : Promise<any> {
      let customUrl = 'http://frenchinstagram2308.cloudapp.net:8888/image/getImage/' + id;
        return this.http.get(customUrl)
           .toPromise()
           .catch(this.handleError);
    }

    addAlbum(name : string) : Promise<any> {
      let username = this.userInfoSerivce.getCurrentUserName();
      let body = {};
      let customUrl = this.urlUpload + 'addAlbum/' + '?albumName=' + name + '&pseudo=' + username;
      return this.http.post(customUrl,body)
               .toPromise()
               .catch(this.handleError);
    }

    updateAlbum(name : string,newName : string) : Promise<any> {
      let username = this.userInfoSerivce.getCurrentUserName();
      let body = {};
      let customUrl = this.urlUpload + 'updateAlbumTitle/' + '?albumName=' + name + '&pseudo=' + username + '&newName=' + newName;
      return this.http.post(customUrl,body)
               .toPromise()
               .catch(this.handleError);
    }

    getAllUserAlbums(id : number) : Promise<any> {
      let customUrl = this.urlUpload + 'getAlbums/' + id;
      return this.http.get(customUrl)
               .toPromise()
               .catch(this.handleError);
    }

    loadAlbum(id : number) : Promise<any> {
      let customUrl = 'http://frenchinstagram2308.cloudapp.net:8888/image/ImagesAlbum/' +id;
      return this.http.get(customUrl)
               .toPromise()
               .catch(this.handleError);
    }

    uploadImage(file : File,title: string,album_name : string) : Promise<any> {
      //let file: File = fileList[0];
        let formData:FormData = new FormData();
        console.log(file);
        let username = this.userInfoSerivce.getCurrentUserName();
        formData.append('fileUpload', file);
        formData.append('album_name',album_name);
        formData.append('pseudo',username);
        formData.append('title',title);
        let headers = new Headers();
       // headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });
        let customUrl = 'http://frenchinstagram2308.cloudapp.net:8888/image/doUpload';
        return this.http.post(customUrl, formData, options)
            .toPromise().then(result => console.log(result));
    }

    addLike(imageId : number) : void {
        let customUrl = 'http://frenchinstagram2308.cloudapp.net:8888/image/likeImage/' + imageId;
        let body = {};
       this.http.post(customUrl,body)
               .toPromise()
               .catch(this.handleError);
    }

    decrementLike(imageId : number) : void {
        let customUrl = 'http://frenchinstagram2308.cloudapp.net:8888/image/dislikeImage/' + imageId;
        let body = {};
       this.http.post(customUrl,body)
               .toPromise()
               .catch(this.handleError);
    }



}

