import { Component } from '@angular/core';
import { Router }      from '@angular/router';

import { ImageAPIService } from '../../services/API/image-api.service';
import { LoginAPIService } from '../../services/API/login-api.service';
import { SocialAPIService } from '../../services/API/social-api.service';
import { UserInfoService } from '../../services/auth/user-info.service';
import { AuthService } from '../../services/auth/auth.service';
import { User } from '../../classes/user';


@Component({
  selector: 'app-toolbar',
  templateUrl: './app.toolbar.html',
  styleUrls: ['./app.toolbar.css'],
})
export class AppToolbar {
  title = 'app works!';
  searchWord : string;
  username : string;

  constructor(public imageAPIService : ImageAPIService,public loginAPIService : LoginAPIService,
              public socialAPIService : SocialAPIService,public userInfoService : UserInfoService,public authService : AuthService,
              private router : Router) {
    console.log(this.authService);
  }

  setProfileValueAtOwn() : void {
      this.userInfoService.setOwnProfileToView();
  }

  search() : void {
    this.username = this.searchWord;
    this.loginAPIService.searchUser(this.searchWord).then(result => this.interpretUserResult(result));
  }

  interpretUserResult(result : Object) : void {
    if (result == null) {
      this.router.navigate(['/home']);
    }
    else {
      let user = JSON.parse(result['_body']);
      this.userInfoService.setUserToView(new User(user['id'],this.username));
      if (this.userInfoService.getRoute() === '/profile'){
        this.router.navigate(['/profile/'+ this.username]);
        this.userInfoService.setRoute('other');
      }
      else {
        this.router.navigate(['/profile']);
        this.userInfoService.setRoute('/profile');
      }
      
    }
  }

}
