import { Component } from '@angular/core';
import { Router }      from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { UserInfoService } from '../../services/auth/user-info.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
})
export class LoginComponent {
    username : string;
    password : string;

    constructor(public authService: AuthService, public router: Router,private userInfoService : UserInfoService) {
    }

    login() {
      this.authService.login(this.username,this.password).then(() => {
        if (this.authService.isLoggedIn) {
          this.userInfoService.setCurrentUser();
          let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/home';
          // Redirect the user
          console.log(redirect);
          this.router.navigate([redirect]);
        }
      });
    }
    logout() {
      this.authService.logout();
    }
}
