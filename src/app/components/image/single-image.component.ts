import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { ImageDataService } from '../../services/data/image.data.service';
import { SocialDataService } from '../../services/data/social.data.service';
import { UserInfoService } from '../../services/auth/user-info.service';
import { Image } from '../../classes/image';
import { Comment } from '../../classes/comment';


@Component({
  selector: 'custom-image',
  templateUrl: './single-image.html',
  styleUrls: ['./single-image.css']
})
export class SingleImageComponent implements OnInit{
    image : Image;
    comments : Array<Comment>;
    displayAddComment : boolean;
    commentContent : string;

    constructor(private imageDataService : ImageDataService,private route: ActivatedRoute,private socialDataService : SocialDataService,
                private userInfoServce : UserInfoService) {
        this.displayAddComment  = false;
    }


    ngOnInit() : void {
      let id : number;
      this.route.params.subscribe(params => {
        if (params['id']) {
          id = params['id']
        }
      });
      this.imageDataService.getSingleImage(id).then(result => {this.image = result;});
      this.socialDataService.getCommentsForImage(id).then(result => {this.comments = result;});
    }

    addComment() : void {
        let senderName = this.userInfoServce.getCurrentUserName();
        this.socialDataService.addComment(senderName,this.image.getId(),this.commentContent);
        let comment = new Comment();
        comment.content = this.commentContent;
        comment.pseudo = this.userInfoServce.getCurrentUserName();
        this.comments.push(comment);
       // this.socialDataService.getCommentsForImage(this.image.getId()).then(result => {this.comments = result;});
        this.displayAddComment = false;
    }

    addLike() : void {

      this.imageDataService.addLike(this.image.getId());
      this.image.incrementLike();
    }

    addDislike() : void {

      this.imageDataService.decrementLike(this.image.getId());
      this.image.decrementLike();
    }

    ngOnDestroy() : void {
    }
    



}
