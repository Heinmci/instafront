import { Injectable} from '@angular/core';

import { Image } from '../../classes/image';
import { User } from '../../classes/user';
import { Comment } from '../../classes/comment';
import { SocialAPIService } from '../API/social-api.service'; 


@Injectable()
export class SocialDataService {
    
    
    constructor(private socialAPIService : SocialAPIService) {
    }

    getCommentsForImage(imageId : number) : Promise<Comment[]> {
        return this.socialAPIService.getCommentsForImage(imageId).then(result => { return result = this.interpretCommentsForImage(result);});
    }

    interpretCommentsForImage(result : Object) : Comment[] {
        let comments : Comment[] = new Array<Comment>();
        let body  = JSON.parse(result['_body']);
        console.log(body);
        for(let element of body) {
            let comment = new Comment();
            comment.content = element['comment'];
            comment.pseudo = element['user']['pseudo'];
            comment.id = element['id'];
            comment.imageTitle = element['image']['title'];
            console.log(comment.imageTitle);
            comments.push(comment);
        }
     
        return comments;
    }

    addComment(senderName : string,imageId : number, comment : string) : void {
        this.socialAPIService.addComment(senderName,imageId,comment);
    }

    updateComment(idComment : number,message : string) : Promise<void> {
        return this.socialAPIService.updateComment(idComment,message).then(() => {return null});
    }

    getAllComments(id : number) : Promise<Comment[]> {
        return this.socialAPIService.getCommentsForUser(id).then(result => { return result = this.interpretCommentsForImage(result);});
    }

    getFriendList(userId : number) : Promise<User[]> {
        return this.socialAPIService.getFriendList(userId).then(result => { return result = this.interpretFriendListResult(result);});
    }

    interpretFriendListResult(result : Object) : User[] {
         console.log(result);
        let friendList : User[] = new Array<User>();
        let body  = JSON.parse(result['_body']);
        for(let element of body) {
            let user = new User(element['user'],element['pseudo']);
            friendList.push(user);
        }

       
       
        return friendList;
    }

    areTheyFriends(userId : number,otherId : number) : Promise<boolean> {
        return this.socialAPIService.areTheyFriends(userId,otherId).then(result => { return result = this.interpretAreTheyFriends(result);});
    }

    interpretAreTheyFriends(result : Object) : boolean {
        let areTheyFriends = false;
        let body  = JSON.parse(result['_body']);
        return body['result'];
    }


}

