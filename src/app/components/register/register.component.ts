import { Component,OnInit } from '@angular/core';
import { Router }      from '@angular/router';
import { RegistrationUser } from '../../classes/user';
import { LoginAPIService } from '../../services/API/login-api.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.css']
})
export class RegisterComponent implements OnInit{

    user : RegistrationUser;
    passwordCheck : string;

    constructor(private loginAPIservice : LoginAPIService, private router : Router) {

    }

    registerUser() : void {
        this.loginAPIservice.registerUser(this.user).then(() => this.router.navigate(['/login']));
    }

    ngOnInit() {
        this.user = new RegistrationUser();
    }

}
