import { User } from './user';

export class Comment {
    content : string;
    pseudo : string;
    id : number;
    imageTitle : string;

    constructor() {
        
    }

    
    public getContent() : string {
        return this.content;
    }

    public setContent(text : string) : void {
        this.content = text;
    }
}