import { Component } from '@angular/core';
import { Router} from '@angular/router';

import { SocialAPIService } from '../../services/API/social-api.service';
import { UserInfoService } from '../../services/auth/user-info.service';

import { User } from '../../classes/user';


@Component({
  selector: 'send-message',
  templateUrl: './send-message.html',
  styleUrls: ['./send-message.css'],
})
export class SendMessageComponent {

  recipient : User;
  messageContent : string;


  constructor(public socialAPIService : SocialAPIService,public userInfoServce : UserInfoService,private router : Router) {

  }

  ngOnInit() {
    this.recipient = this.userInfoServce.getUserToSendMessageTo();
  }

  sendMessage() : void {
    let senderId = this.userInfoServce.getCurrentUserId();
    this.socialAPIService.sendMessage(this.messageContent,this.recipient.getId(),senderId);
    this.router.navigate(['/profile']);
  }



  
}
