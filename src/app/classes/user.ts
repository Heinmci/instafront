export class User {

    private username : string;
    private id : number;
    private token : string;

    constructor (id : number, name : string) {
        this.username = name;
        this.id = id;
    }

    public getId() : number {
        return this.id;
    }

    public getUserName() : string {
        return this.username;
    }

    public setToken(token : string) {
        this.token = token;
    }

    public getToken() : string {
        return this.token;
    }
}

export class RegistrationUser {
    private pseudo : string;
    private email : string;
    private password : string;

}