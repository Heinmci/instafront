import { Injectable } from '@angular/core';


import { User } from '../../classes/user';

@Injectable()
export class UserInfoService {

  userToView: User;
  userToSendMessageTo : User;
  currentUser : User;
  route : string = '';

  setCurrentUser() : void {
    let user = JSON.parse(localStorage.getItem('currentUser'));
    console.log(user);
    this.currentUser = new User(user['id'],user['username']);
    this.currentUser.setToken(user['token']);
    this.userToView = this.currentUser;
  }

  getCurrentUserToken() : string {
    return this.currentUser.getToken();
  }

  getCurrentUserId() : number {
    return this.currentUser.getId();
  }

  setRoute(route : string) : void {
    this.route = route;
  }

  getRoute() : string {
    return this.route;
  }

  getUser() : User {
    return this.currentUser;
  }

  getCurrentUserName() : string {
    return this.currentUser.getUserName();
  }

  getUserToView() : User {
    return this.userToView;
  }

  setUserToView(user : User) : void {
    this.userToView = user;
  }

  setOwnProfileToView() : void {
    this.userToView = this.currentUser;
  }

  setUserToSendMessageTo(user : User) : void {
    this.userToSendMessageTo = user;
  }

  getUserToSendMessageTo() : User {
    return this.userToSendMessageTo;
  }
}

