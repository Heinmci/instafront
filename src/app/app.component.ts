import { Component } from '@angular/core';
import { ImageAPIService } from './services/API/image-api.service';
import { LoginAPIService } from './services/API/login-api.service';
import { SocialAPIService } from './services/API/social-api.service';
import { UserInfoService } from './services/auth/user-info.service';
import { ImageDataService } from './services/data/image.data.service';
import { SocialDataService } from './services/data/social.data.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers : [ImageAPIService, LoginAPIService, SocialAPIService,UserInfoService,ImageDataService,SocialDataService]
})
export class AppComponent {
  title = 'app works!';
}
