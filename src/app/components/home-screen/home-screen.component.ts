import { Component, OnInit } from '@angular/core';
import { ImageDataService } from '../../services/data/image.data.service';
import { Image } from '../../classes/image';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'home-screen',
  templateUrl: './home-screen.component.html',
  styleUrls: ['./home-screen.css']
})
export class HomeScreenComponent implements OnInit{
    images : Array<Image>;
    imageString : any;
    imgSrc : string;


    constructor(private sanitizer:DomSanitizer,public imageDataService : ImageDataService) {
        
    }


    interpesretTEst(result : any) : void {
        // window.blobUtil.binaryStringToBlob(result['body']).then(function (blob) {
        //   blobUtil.blobToBase64String(blob).then(function (base64String) {
        //       this.imgSrc = 'data:image/;base64,' + base64String; 
        //     }).catch(function (err) {
        //       // error 
        //     }); 
        // }).catch(function (err) {
        //   // error 
        // });
    }

    interpretResult(result : any) : void {
        console.log(result);
        // let base64 = btoa(encodeURIComponent(result['_body']));
        // this.imageString = base64;
        // console.log(this.imageString);
        this.imageString = this.hexToBase64(result['_body']);
        console.log(this.imageString);
        this.imgSrc = 'data:image/;base64,' + this.imageString;
    }

    getSrc() : any {
        return this.sanitizer.bypassSecurityTrustUrl(this.imgSrc);
    }

     hexToBase64(str : any) : any{
        return btoa(String.fromCharCode.apply(null, str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" ")));
    }

    ngOnInit() : void {
      //  this.imageAPIService.getHomeSreenImages().then(result => this.dealWithImages(result))
        this.imageDataService.getHomeSreenImages().then(result => {this.images = result;console.log(this.images)});

    }



}
