import { User } from './user';
import { Image } from './image';

export class Album {
    title : string;
    id : number;
    user : User;
    images : Image[];

    constructor () {

    }

    public getId() : number {
        return this.id;
    }
}