import { Injectable} from '@angular/core';

import { Image } from '../../classes/image';
import { Album } from '../../classes/album';
import { ImageAPIService } from '../API/image-api.service'; 


@Injectable()
export class ImageDataService {
    
    imageList : Array<Image>;
    result : Object;    
    albumName : string;
    
    constructor(private imageAPIService : ImageAPIService) {
    }

    getHomeSreenImages() : Promise<Image[]> {
        return this.imageAPIService.getHomeSreenImages().then(result => {return result = this.interpretImageList(result);});
        //return this.interpretImageList();
    }

    setAlbumName(name : string) : void {
        this.albumName = name;
    }

    getAlbumName() : string {
        return this.albumName;
    }

    interpretImageList(result : Object) : Image[] {
        let obj = JSON.parse(result['_body']);
        let imgList = new Array<Image>();
        for (let img of obj) {


            let image = new Image();
            var parts = img['title'].split('.');
            console.log(parts);
            image.setExtension(parts[parts.length-1]);
            image.setSrc('data:image/' + image.getExtension() + ';base64,' + img['datas']);
            //console.log(image.getSrc());
            
            image.setId(img['id']);
            image.setDislike(img['dislikescore']);
            image.setLike(img['likescore']);
            image.setTitle(img['title']);
            imgList.push(image);

        }
        console.log(JSON.parse(result['_body']));
        return imgList;
    }

    getSingleImage(id : number) : Promise<Image> {
        return this.imageAPIService.getSingleImage(id).then(result => { return result = this.interpretSingleImage(result);});
    }

    interpretSingleImage(result : Object) : Image {
        let image : Image = new Image();
        let img = JSON.parse(result['_body']);
        var parts = img['title'].split('.');
        console.log(img);
        image.setExtension(parts[parts.length-1]);
        image.setSrc('data:image/' + image.getExtension() + ';base64,' + img['datas']);
        //console.log(image.getSrc());
        
        image.setId(img['id']);
        image.setDislike(img['dislikescore']);
        image.setLike(img['likescore']);
        image.setTitle(img['title']);
        image.setAuthor(img['album']['user']['pseudo']);
        return image;
    }

    addAlbum(name : string) : Promise<any> {
        return this.imageAPIService.addAlbum(name);
    }

    getAllUserAlbums(id : number) : Promise<Album[]> {
        return this.imageAPIService.getAllUserAlbums(id).then(result => { return result = this.interpretAlbumList(result);});
    }

    interpretAlbumList(result : Array<Object>) : Album[] {
        let body  = JSON.parse(result['_body']);
        let albumList = new Array<Album>();
        for (let i = 0; i < body.length; i++) {
            let album = new Album();
            album.title = body[i]['title'];
            album.id = body[i]['id'];
            albumList.push(album);
        }
        console.log(albumList);
        return albumList;
    }

    uploadImage(image : File,title : string,albumName : string) {
        this.imageAPIService.uploadImage(image,title,albumName);
    }

    addLike(imageId : number) : void {
        this.imageAPIService.addLike(imageId);
    }

    decrementLike(imageId : number) : void {
        this.imageAPIService.decrementLike(imageId);
    }

    loadAlbum(id : number) : Promise<Album> {
        return this.imageAPIService.loadAlbum(id).then(result => { return result = this.interpretAlbum(result);});
    }

    interpretAlbum(result : Object) : Album {
        let body = JSON.parse(result['_body']);
        console.log(body);
        var album = new Album();
        album.title = this.albumName;
        album.images = new Array<Image>();

        for(let img of body) {
            let image = new Image();
            var parts = img['title'].split('.');
            image.setExtension(parts[parts.length-1]);
            image.setSrc('data:image/' + image.getExtension() + ';base64,' + img['datas']);
            image.setId(img['id']);
            image.setDislike(img['dislikescore']);
            image.setLike(img['likescore']);
            image.setTitle(img['title']);
            album.images.push(image);
        }

        return album;
    }


}

