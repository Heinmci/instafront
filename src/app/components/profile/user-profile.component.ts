import { Component } from '@angular/core';
import { UserInfoService } from '../../services/auth/user-info.service';
import { SocialAPIService } from '../../services/API/social-api.service';
import { ImageDataService } from '../../services/data/image.data.service';
import { SocialDataService } from '../../services/data/social.data.service';
import { Image } from '../../classes/image';
import { Album } from '../../classes/album';
import { User } from '../../classes/user';
import { Comment } from '../../classes/comment';
import { DomSanitizer } from '@angular/platform-browser';
import { Router} from '@angular/router';


@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./profile.css'],
})
export class UserProfileComponent {
    user : User;
    ownProfile : boolean;
    isFriend : boolean;
    isOwnProfile : boolean;
    friendList : Array<User>;
    albums : Album[];
    comments : Comment[];
    


    constructor(private socialAPIService : SocialAPIService,private socialDataService : SocialDataService,private imageDataService : ImageDataService,private userInfoService : UserInfoService,
                private router : Router) {
        this.isFriend = false;
        this.user = this.userInfoService.getUserToView();
        this.checkOwnProfile();
        this.loadFriendList();
        this.loadAlbums();
        this.loadComments();
    }

    checkFriends() : void {
        this.socialDataService.areTheyFriends(this.userInfoService.getCurrentUserId(),this.user.getId()).then(result => {this.isFriend = result;console.log(result)});

    }

    checkOwnProfile() : void {
        if (this.user.getId() == this.userInfoService.getCurrentUserId()) {
            this.ownProfile = true;
            this.isFriend = false;
        }
        else {
            this.ownProfile = false;
            this.checkFriends();
        }
    }

    setAlbumName(name : string) {
        this.imageDataService.setAlbumName(name);
    }

    loadComments() : void {
        this.socialDataService.getAllComments(this.user.getId()).then(result => this.comments = result);
    }

    addFriend() : void {
        this.socialAPIService.addFriend(this.userInfoService.getCurrentUserId(),this.user.getId());
        this.friendList.push(new User(this.userInfoService.getCurrentUserId(),this.userInfoService.getCurrentUserName()));
        this.isFriend = true;
    }

    removeFriend() : void {
        this.socialAPIService.removeFriend(this.userInfoService.getCurrentUserId(),this.user.getId());
        this.isFriend = false;
        for(let i = 0; i < this.friendList.length; i++) {
            if (this.friendList[i].getId() === this.userInfoService.getCurrentUserId()) {
                this.friendList.splice(i,1);
                return;
            }
        }
    }

    

    sendMessage() : void {
        this.userInfoService.setUserToSendMessageTo(this.user);
        this.router.navigate(['/sendMessage']);
    }

    loadFriendList() : void {
        this.socialDataService.getFriendList(this.user.getId()).then(result => this.friendList = result);
    }

    loadAlbums() : void {
        this.imageDataService.getAllUserAlbums(this.user.getId()).then(result => {this.albums = result;});
    }

    goToProfile(user : User) : void {
      this.userInfoService.setUserToView(user);
      if (this.userInfoService.getRoute() === '/profile'){
        this.router.navigate(['/profile/'+user.getUserName()]);
        this.userInfoService.setRoute('other');
      }
      else {
        this.router.navigate(['/profile']);
        this.userInfoService.setRoute('/profile');
      }
      
    }

    goToAlbum(album : Album) : void {
        
    }

    



}
