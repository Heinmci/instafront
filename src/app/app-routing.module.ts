import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent }   from './components/login/login.component';
import { RegisterComponent }   from './components/register/register.component';
import { HomeComponent }   from './components/home/home.component';
import { UserProfileComponent } from './components/profile/user-profile.component';
import { SendMessageComponent } from './components/message/send-message.component';
import { SingleImageComponent } from './components/image/single-image.component';
import { UploadImageComponent } from './components/upload/upload.component';
import { AlbumComponent } from './components/album/album.component';

import { AuthGuard } from './services/auth/auth-guard.service';
import { AuthService } from './services/auth/auth.service';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login',  component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent },
  { path : 'profile', component : UserProfileComponent,canActivate : [AuthGuard]},
  { path : 'profile/:pseudo', component : UserProfileComponent,canActivate : [AuthGuard]},
  { path : 'myProfile', component : UserProfileComponent,canActivate : [AuthGuard]},
  { path : 'sendMessage', component : SendMessageComponent,canActivate : [AuthGuard]},
  { path : 'image/:id', component : SingleImageComponent},
  { path : 'upload', component : UploadImageComponent,canActivate : [AuthGuard]},
  { path : 'album/:id', component : AlbumComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  providers : [AuthGuard,AuthService]
})
export class AppRoutingModule {
}