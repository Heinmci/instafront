import { Injectable} from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { UserInfoService } from '../auth/user-info.service';

import { Subject }    from 'rxjs/Subject';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class SocialAPIService {

     url : string = 'http://frenchinstagram2308.cloudapp.net:8888/friends/';

    constructor(private http: Http,private userInfoService : UserInfoService) { 
    }

    areTheyFriends (userId : number, profileId : number) : Promise<any> {
      let customUrl = this.url + 'areTheyFriends?id_user=' + userId + '&id_friend=' + profileId; 
       return this.http.get(customUrl).toPromise()
               .catch(this.handleError);
    }

    addFriend (userId : number, otherId : number) : void {
       let customUrl = this.url + 'addFriend?id_user=' + userId + '&id_friend=' + otherId; 
       this.http.get(customUrl).toPromise()
               .catch(this.handleError);
    }

    removeFriend (userId : number, otherId : number) : void {
        let customUrl = this.url + 'removeFriend?id_user=' + userId + '&id_friend=' + otherId; 
        this.http.get(customUrl)
               .toPromise().then(result => console.log(result))
               .catch(this.handleError);
    }

    sendMessage(messageContent : string,recipientId : number,sendId : number) : void{
        return null;
    }

    searchUser(userKeyword : string) : Promise<any>{
        return null;
    }
    
    getMessageList(userId : number) : Promise<any> {
        return null;
    }

    updateComment(commentId : number,message : string) : Promise<any> {
      let customUrl = 'http://frenchinstagram2308.cloudapp.net:8888/comments/update/' + commentId + '/' + message;
        console.log(customUrl);
        return this.http.post(customUrl,{})
                .toPromise()
                .catch(this.handleError);
    }

    getCommentsForUser(id : number) : Promise<any> {
      let customUrl = 'http://frenchinstagram2308.cloudapp.net:8888/comments/user/' + id;
        return this.http.get(customUrl)
               .toPromise()
               .catch(this.handleError);
    }

    getFriendList(userId : number) : Promise<any> {
        let customUrl = this.url + 'friends?id_user=' + userId; 
        return this.http.get(customUrl)
               .toPromise()
               .catch(this.handleError);
    }

    getCommentsForImage(imageId : number) : Promise<any> {
      let customUrl = 'http://frenchinstagram2308.cloudapp.net:8888/comments/image/' + imageId;
        return this.http.get(customUrl)
               .toPromise()
               .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    addComment(senderName : string,imageId : number, comment : string) : Promise<any> {
        let customUrl = 'http://frenchinstagram2308.cloudapp.net:8888/comments/' + senderName + '/' + imageId + '/' + comment;//+ '?token=' + this.userInfoService.getCurrentUserToken();
        console.log(customUrl);
        return this.http.post(customUrl,{})
                .toPromise()
                .catch(this.handleError);                                                  
    }


}

