import { Injectable} from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';

import { Subject }    from 'rxjs/Subject';
import 'rxjs/add/operator/toPromise';

import { RegistrationUser } from '../../classes/user';


@Injectable()
export class LoginAPIService {

    url : string = 'http://frenchinstagram2308.cloudapp.net:8888/users/';

    constructor(private http: Http) { 
    }

    registerUser(user : RegistrationUser) : Promise<any> {
         let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
        let aser = {
            'pseudo' : 'Bob',
            'password' : 'Bob',
            'email' : 'bob@bob.bob'
        }
        console.log(user);
        return this.http.post(this.url,user,options)
               .toPromise();
    }

    checkLogin(username : string,password : string) : Promise<any> {
      let customUrl  = this.url + '/verify?pseudo=' + username + '&password=' + password;
      return this.http.get(customUrl)
               .toPromise()
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    searchUser(name : string) : Promise<any> {
      let customUrl = this.url + name;
      return this.http.get(customUrl)
                      .toPromise();
    }


}

