import { Component,Input } from '@angular/core';

import { Comment } from '../../classes/comment';

@Component({
  selector: 'comment',
  templateUrl: './app.comment.html',
})
export class CommentComponent {
    @Input() comment : Comment;

    constructor() {
    }
    
}
