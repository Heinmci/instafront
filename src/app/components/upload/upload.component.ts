import { Component,OnInit} from '@angular/core';
import { ImageAPIService } from '../../services/API/image-api.service';
import { ImageDataService } from '../../services/data/image.data.service';
import { SocialAPIService } from '../../services/API/social-api.service';
import { UserInfoService } from '../../services/auth/user-info.service';

import { Album } from '../../classes/album';


@Component({
  selector: 'upload-image',
  templateUrl: './upload-image.html',
  styleUrls: ['./upload-image.css'],
})
export class UploadImageComponent implements OnInit{
  title = 'app works!';
  userAlbumList : Array<Album>;
  albumName : string;
  selectedAlbum : string;
  createOrSelectAlbum : number;
  image : File;

  constructor(public imageDataService : ImageDataService,private userInfoService : UserInfoService) {
    this.createOrSelectAlbum = 1;  
    this.userAlbumList = new Array<Album>();
  }

  addAlbum() : void {

    this.imageDataService.addAlbum(this.albumName).then(() => {this.loadAlbumList(); this.createOrSelectAlbum = 1; })

  }

  loadAlbumList() : void {
    let id = this.userInfoService.getCurrentUserId();
      this.imageDataService.getAllUserAlbums(id).then(result => {this.pushAlbumsToList(result);});
      

  }
  upload() : void {
    this.imageDataService.uploadImage(this.image,this.image.name,this.selectedAlbum);
  }
  pushAlbumsToList(result : Album[]) : void {
    this.userAlbumList = new Array<Album>();
    for (let i = 0; i < result.length; i++) {
      this.userAlbumList.push(result[i]);
    }
  }
  ngOnInit() : void {
      this.loadAlbumList();
  }

  fileSelected(image : File) : void {
    this.image = image;
  }
}
