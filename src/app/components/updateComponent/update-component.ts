import { Component, Input } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { ImageDataService } from '../../services/data/image.data.service';
import { SocialDataService } from '../../services/data/social.data.service';
import { UserInfoService } from '../../services/auth/user-info.service';
import { Image } from '../../classes/image';
import { Comment } from '../../classes/comment';


@Component({
  selector: 'update-comment',
  templateUrl: './update-component.html',
  styleUrls: ['./update.css']
})
export class UpdateCommentComponent{
	@Input() comment : Comment;
	@Input() ownProfile : boolean;
	updateCommentBool : boolean;
    commentContent : string;

	constructor (private socialDataService : SocialDataService) {
		 this.updateCommentBool  = false;
	}

	updateComment(id : number) : void {
        this.socialDataService.updateComment(id,this.commentContent).then(() => this.comment.content = this.commentContent);
        
        this.updateCommentBool = false;
    }

}