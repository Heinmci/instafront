import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { PerfectScrollbarModule } from 'angular2-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'angular2-perfect-scrollbar';

import { AuthService } from './services/auth/auth.service';
import { ImageAPIService } from './services/API/image-api.service';
import { LoginAPIService } from './services/API/login-api.service';
import { SocialAPIService } from './services/API/social-api.service';
import { UserInfoService } from './services/auth/user-info.service';
import { ImageDataService } from './services/data/image.data.service';

import { AppToolbar } from './components/toolbar/app.toolbar.component';
import { LoginComponent }   from './components/login/login.component';
import { RegisterComponent }   from './components/register/register.component';
import { HomeComponent }   from './components/home/home.component';
import { AppComponent } from './app.component';
import { HomeScreenComponent } from './components/home-screen/home-screen.component';
import { UserProfileComponent} from './components/profile/user-profile.component';
import { SendMessageComponent} from './components/message/send-message.component';
import { SingleImageComponent } from './components/image/single-image.component';
import { CommentComponent } from './components/comment/app.comment.component';
import { UploadImageComponent } from './components/upload/upload.component';
import { AlbumComponent } from './components/album/album.component';
import { UpdateCommentComponent } from './components/updateComponent/update-component';

const PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  scrollYMarginOffset : 5
  // minScrollbarLength : 1,
  // maxScrollbarLength : 11

};

@NgModule({
  declarations: [
    AppComponent,LoginComponent,RegisterComponent,HomeComponent,HomeScreenComponent, UserProfileComponent,AppToolbar,SendMessageComponent,
    SingleImageComponent,CommentComponent,UploadImageComponent,AlbumComponent,UpdateCommentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    AppRoutingModule,
    PerfectScrollbarModule.forRoot(PERFECT_SCROLLBAR_CONFIG)
  ],
  providers: [ImageAPIService,LoginAPIService,SocialAPIService,UserInfoService,ImageDataService,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
