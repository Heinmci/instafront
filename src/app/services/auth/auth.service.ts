import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

import { User } from '../../classes/user';
import { LoginAPIService } from '../API/login-api.service';

@Injectable()
export class AuthService {
  isLoggedIn: boolean = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string = '/home';

  constructor(private loginAPIService : LoginAPIService) {

  }

  login(username : string,password : string): Promise<any> {
    
    return this.loginAPIService.checkLogin(username,password).then(result => {return this.doLoginStuff(result,username);} );
    // return Observable.of(true).delay(1000).do(val => this.isLoggedIn = true);
  }

  doLoginStuff(result : Object,username : string) : Object {
    console.log(result,result['_body']['id']);
    let body  = JSON.parse(result['_body']);

    if (body['id'] != null) {
        this.isLoggedIn = true; 
        localStorage.setItem('currentUser', JSON.stringify({ username: username, token: body['token'],id: body['id'] }));
        console.log(localStorage.getItem('currentUser'));
    }
    return result;
  }
  logout(): void {
    this.isLoggedIn = false;
  }

  getCurrentUserId() : number {
    return 12; // localStorage.getItem('currentUser')
  }

  getUser() : User {
    return new User(this.getCurrentUserId(),this.getCurrentUserName());
  }

  getCurrentUserName() : string {
    return 'Bob';
  }
}

// login(username: string, password: string): Observable<boolean> {
//         return this.http.post('/api/authenticate', JSON.stringify({ username: username, password: password }))
//             .map((response: Response) => {
//                 // login successful if there's a jwt token in the response
//                 let token = response.json() && response.json().token;
//                 if (token) {
//                     // set token property
//                     this.token = token;
 
//                     // store username and jwt token in local storage to keep user logged in between page refreshes
//                     localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
 
//                     // return true to indicate successful login
//                     return true;
//                 } else {
//                     // return false to indicate failed login
//                     return false;
//                 }
//             });
//     }
 
//     logout(): void {
//         // clear token remove user from local storage to log user out
//         this.token = null;
//         localStorage.removeItem('currentUser');
//     }