import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { ImageDataService } from '../../services/data/image.data.service';
import { SocialDataService } from '../../services/data/social.data.service';
import { UserInfoService } from '../../services/auth/user-info.service';
import { Album } from '../../classes/album';
import { Comment } from '../../classes/comment';


@Component({
  selector: 'album',
  templateUrl: './album.html',
  styleUrls: ['./album.css']
})
export class AlbumComponent implements OnInit{
    album : Album;
    

    constructor(private imageDataService : ImageDataService,private route: ActivatedRoute,private socialDataService : SocialDataService,
                private userInfoServce : UserInfoService) {
       
    }

    ngOnInit() : void {
      let id : number;
      this.route.params.subscribe(params => {
        if (params['id']) {
          id = params['id']
        }
      });
      this.imageDataService.loadAlbum(id).then(result => this.album = result);
    }

    



}
